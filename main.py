__author__ = 'Mohammed Alhajeri (Label Agency)'
from wand.image import Image
from PIL import  Image as PImage
import sys
import datetime
import json
import os
class CreatePDF():

    MEDIA_URL="http://app.labelag.com/"


    def __init__(self,path):
        self.path = path
        self.report_file = open(path+'report.txt','w')
        self.id = 1
        self.final_list = []

    def split(self,source_file,destination_folder):
        myDic = {}
        myDic['id']=self.id
        myDic["title"]=os.path.splitext(os.path.basename(source_file))[0]
        myDic["date"]=str(datetime.datetime.now())
        myDic["section"]=os.path.dirname(destination_folder).split('/')[-1]
        myDic["type"]="document"
        myDic["mime-type"]="application/pdf"
        myDic["file"]= self.MEDIA_URL+myDic["section"]+'/'+os.path.basename(source_file)
        print myDic["file"]
        self.final_list.append(myDic)
        self.id = self.id +1




    def traverse(self):
        for root, dirs, files in os.walk(self.path):
           if root.startswith("/Users/q80/Documents/KNA-Final/cvs"):
               pass
           else:
               for file in files:
                   if file.endswith(".pdf"):
                       self.split(os.path.join(root, file),root)
        with open(self.path+'/index.json', 'w') as jsonfile:
            json.dump(self.final_list,jsonfile)

    def clean(self):
        for root, dirs, files in os.walk(self.path):
            for file in files:
                if (file.endswith(".jpg") and file.startswith("file")) or file.endswith(".json"):
                     os.remove(os.path.join(root, file))



if __name__ == "__main__":
    obj = CreatePDF(sys.argv[1])
    #obj.clean()
    obj.traverse()
    print "Done Successfully"